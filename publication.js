'use strict';
Vazco.Access.publish = function(params) {
    var associations, collection, collectionHandle, filter, options, pub, userObj, publishedDocsCounts = {};
    pub = params.handle;
    collection = params.collection;
    associations = params._associations || {};
    userObj = Meteor.users.findOne(pub.userId);
    var doMapping = function(id, obj, mappings) {
        var mapFilter, mapOptions, mapping, objKey;
        if (!mappings) {
            return;
        }
        for (var _i = 0, _len = mappings.length; _i < _len; _i++) {
            mapping = mappings[_i];
            mapFilter = {};
            mapOptions = {};
            if (mapping.reverse) {
                objKey = mapping.collection._name;
                mapFilter[mapping.key] = id;
            } else {
                objKey = mapping.key;
                mapFilter._id = obj[mapping.key];
                if (_.isArray(mapFilter._id)) {
                    mapFilter._id = {
                        $in: mapFilter._id
                    };
                }
            }
            _.extend(mapFilter, mapping.filter);
            _.extend(mapOptions, mapping.options);
            associations[id] = associations[id] || {};
            var _handlr = UniUtils.get(associations, id + '.' + objKey);
            if (_handlr) {
                _handlr.stop();
            }
            try {
                if (mapping.mappings && mapping.mappings.length) {
                    associations[id][objKey] = publishWithMappings(mapping.collection, mapFilter, mapOptions, mapping.mappings);
                    //Vazco.Access.publish({
                    //    handle: pub,
                    //    collection: mapping.collection,
                    //    filter: mapFilter,
                    //    options: mapOptions,
                    //    mappings: mapping.mappings,
                    //    _associations: associations
                    //});
                } else {
                    associations[id][objKey] = publishSimple(mapping.collection, mapFilter, mapOptions);
                }
            } catch (e) {
                console.error(pub._name, ':', e.message);
            }
        }
    };
    var addedHandler = function(document, collection) {
        if (!publishedDocsCounts[document._id+collection._name]) {
            publishedDocsCounts[document._id+collection._name] = 0;
        }
        publishedDocsCounts[document._id+collection._name]++;
        if (pub._documents && pub._documents[collection._name] && pub._documents[collection._name][document._id]) {
            return;
        }
        if (Vazco.Access.resolve('show', document, userObj, collection)) {
            pub.added(collection._name, document._id, document);
        }
        return true;
    };
    var changedHandler = function(newDocument, oldDocument, collection) {
        var diff, newAccess = Vazco.Access.resolve('show', newDocument, userObj, collection);
        if (!newAccess) {
            return removedHandler(newDocument, collection);
        }

        if(!pub._documents[collection._name] || !pub._documents[collection._name][newDocument._id]) {
            return addedHandler(newDocument, collection);
        }

        diff = Vazco.Access.diff(oldDocument, newDocument);
        pub.changed(collection._name, newDocument._id, _.omit(diff, '_id'));
        return true;
    };

    var removedHandler = function(oldDocument, collection) {
        if (publishedDocsCounts[oldDocument._id+collection._name]) {
            publishedDocsCounts[oldDocument._id+collection._name]--;
        }
        if (publishedDocsCounts[oldDocument._id+collection._name] > 0) {
            return;
        }
        delete publishedDocsCounts[oldDocument._id+collection._name];
        if(!pub._documents[collection._name] || !pub._documents[collection._name][oldDocument._id]) {
            return;
        }
        return pub.removed(collection._name, oldDocument._id);
    };
    var publishSimple = function(collection, filter, options) {
        options = options || {};
        return collection.find(filter, options).observe({
            added: function (document) {
                return addedHandler(document, collection);
            },
            changed: function (newDocument, oldDocument) {
                return changedHandler(newDocument, oldDocument, collection);
            },
            removed: function (oldDocument) {
                return removedHandler(oldDocument, collection);
            }
        });

    };
    var publishWithMappings = function (collection, filter, options, mappings) {
        options = options || {};
        return collection.find(filter, options).observe({
            added: function(document) {
                var id = document._id;
                if (addedHandler( document, collection)) {
                    doMapping(id, document, mappings);
                }
            },
            changed: function(newDocument, oldDocument) {
                if (changedHandler(newDocument, oldDocument, collection)) {
                    return _.each(newDocument, function(value, key) {
                        var changedMappings;
                        changedMappings = _.filter(mappings, function(mapping) {
                            return mapping.key === key && !mapping.reverse;
                        });
                        doMapping(newDocument._id, newDocument, changedMappings);
                    });
                }
            },
            removed: function(oldDocument) {
                var handle, _i, _len, _ref;
                if (oldDocument) {
                    removedHandler(oldDocument, collection);
                    _ref = associations[oldDocument._id];
                    if(_ref && _ref.length) {
                        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                            handle = _ref[_i];
                            handle && handle.stop();
                            delete _ref[_i];
                        }
                        delete associations[oldDocument._id];
                    }
                }
            }
        });
    };
    filter = params.filter || {};
    options = params.options || {};

    if (params.mappings && params.mappings.length > 0) {
        collectionHandle = publishWithMappings(collection, filter, options, params.mappings);
    } else {
        collectionHandle = publishSimple(collection, filter, options);
    }

    if (!params._associations) {
        pub.onStop(function() {
            var association, handle;
            for (var id in associations) {
                association = associations[id];
                for (var key in association) {
                    handle = association[key];
                    handle && handle.stop();
                    delete association[key];
                }
                delete associations[id];
            }
        });
        if (!params._noReady) {
            pub.ready();
        }
    }

    if (collectionHandle) {
        pub.onStop(function() {
            collectionHandle.stop();
        });
    }
};
